<?php

namespace App\Enum;

abstract class BasketStatusEnum
{
    const STATUS_CREATED = '100';
    const STATUS_CANCELED = '200';
    const STATUS_VALIDATED = '300';
    const STATUS_IN_PREPARATION = '400';
    const STATUS_SHIPPED = '500';
    const STATUS_RECEIVED = '600';
    const STATUS_REFUNDED = '700';
}
