<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PaymentRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: [
        "get" => ["security" => "is_granted('ROLE_STATS')"],
        ]
)]
class Payment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(length: 40)]
    #[
        Assert\NotBlank,
        Assert\Length([
            'min' => 2,
            'max' => 40,
            'minMessage' => 'Le nom du mode de paiement doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom du mode de paiement doit faire au maximum {{ limit }} caractères',
        ]),
    ]
    private ?string $type = null;

    #[ORM\OneToMany(mappedBy: 'payment', targetEntity: Basket::class)]
    private Collection $baskets;



    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->baskets = new ArrayCollection();
    }



   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, Basket>
     */
    public function getBaskets(): Collection
    {
        return $this->baskets;
    }

    public function addBasket(Basket $basket): self
    {
        if (!$this->baskets->contains($basket)) {
            $this->baskets[] = $basket;
            $basket->setPayment($this);
        }

        return $this;
    }

    public function removeBasket(Basket $basket): self
    {
        if ($this->baskets->removeElement($basket)) {
            // set the owning side to null (unless already changed)
            if ($basket->getPayment() === $this) {
                $basket->setPayment(null);
            }
        }

        return $this;
    }




}
