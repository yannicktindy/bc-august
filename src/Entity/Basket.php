<?php

namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiResource;
use App\Enum\BasketStatusEnum;
use App\Repository\BasketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use App\Controller\Back\Api\AverageOrderController;
use App\Controller\Back\Api\BasketToOrderController;
use App\Controller\Back\Api\CanceledBasketController;
use App\Controller\Back\Api\NbBasketController;
use App\Controller\Back\Api\NbOrderController;
use App\Controller\Back\Api\TotalSoldController;
use App\Controller\Back\Api\VisitToBasketController;

#[ORM\Entity(repositoryClass: BasketRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => ["security" => "is_granted('ROLE_STATS')"], 
        'get_NumberBasket' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/NumberBasket',
            'controller'=> NbBasketController::class,
        ],
        'get_NumberOrder' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/NumberOrder',
            'controller'=> NbOrderController::class,
        ],
        'get_CanceledBasket' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/CanceledBasket',
            'controller'=> CanceledBasketController::class,
        ],
        'get_ConversionVisitToBasket' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/ConversionVisitToBasket',
            'controller'=> VisitToBasketController::class,
        ],
        'get_ConversionVisitToBasket' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/ConversionVisitToBasket',
            'controller'=> BasketToOrderController::class,
        ],
        'get_TotalSold' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/TotalSold',
            'controller'=> TotalSoldController::class,
        ],
        'get_AverageOrder' => [
            'method'   => 'GET',
            'security' => ' is_granted("ROLE_STATS")',
            "defaults" => ["_format" => "json"],
            'path' => '/stats/AverageOrder',
            'controller'=> AverageOrderController::class,
        ],
    ],
    itemOperations: [
        'get' => [
            'security' => ' is_granted("ROLE_STATS")',
            'method'   => 'GET',
            
        ],
    ]
)]
class Basket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;



    #[ORM\OneToMany(mappedBy: 'basket', targetEntity: Line::class)]
    private Collection $bLine;

    #[ORM\ManyToOne(inversedBy: 'baskets')]
    private ?User $user = null;

    #[ORM\Column(length: 255)]

    private string $status = BasketStatusEnum::STATUS_CREATED;

    #[ORM\ManyToOne(inversedBy: 'baskets')]
    #[
        Assert\NotBlank,
        
    ]
    private ?Payment $payment = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $datePay = null;

    #[ORM\ManyToOne(inversedBy: 'baskets')]
    private ?Adress $adress = null;

    public function __construct()
    {
        $this->bLine = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return Collection<int, Line>
     */
    public function getBLine(): Collection
    {
        return $this->bLine;
    }

    public function addBLine(Line $bLine): self
    {
        if (!$this->bLine->contains($bLine)) {
            $this->bLine[] = $bLine;
            $bLine->setBasket($this);
        }

        return $this;
    }

    public function removeBLine(Line $bLine): self
    {
        if ($this->bLine->removeElement($bLine)) {
            // set the owning side to null (unless already changed)
            if ($bLine->getBasket() === $this) {
                $bLine->setBasket(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDatePay(): ?\DateTimeInterface
    {
        return $this->datePay;
    }

    public function setDatePay(?\DateTimeInterface $datePay): self
    {
        $this->datePay = $datePay;

        return $this;
    }

    public function getAdress(): ?Adress
    {
        return $this->adress;
    }

    public function setAdress(?Adress $adress): self
    {
        $this->adress = $adress;

        return $this;
    }
}
