<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Back\Api\AllStatisticController;
use App\Repository\StatisticRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StatisticRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get_statistic' => [
            // Limit access to get item operation only if the logged user is one of:
            // - have ROLE_ADMIN
            'security' => ' is_granted("ROLE_STATS")',
            'method'   => 'GET',
            'path' => '/stats/statistic',
            'controller'=> AllStatisticController::class,
        ],
    ],
    itemOperations: [
        'get' => [
            'security' => ' is_granted("ROLE_STATS")',
            'method'   => 'GET',
            
        ],
    ]
)]
class Statistic
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endDate = null;

    #[ORM\Column]
    private ?int $nbVisit = null;

    #[ORM\Column]
    private ?int $nbBasket = null;

    #[ORM\Column]
    private ?int $nbOrder = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $totalSum = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getNbVisit(): ?int
    {
        return $this->nbVisit;
    }

    public function setNbVisit(int $nbVisit): self
    {
        $this->nbVisit = $nbVisit;

        return $this;
    }

    public function getNbBasket(): ?int
    {
        return $this->nbBasket;
    }

    public function setNbBasket(int $nbBasket): self
    {
        $this->nbBasket = $nbBasket;

        return $this;
    }

    public function getNbOrder(): ?int
    {
        return $this->nbOrder;
    }

    public function setNbOrder(int $nbOrder): self
    {
        $this->nbOrder = $nbOrder;

        return $this;
    }

    public function getTotalSum(): ?string
    {
        return $this->totalSum;
    }

    public function setTotalSum(string $totalSum): self
    {
        $this->totalSum = $totalSum;

        return $this;
    }
}
