<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Back\Api\NbVisitController;
use App\Repository\VisitRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VisitRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get_visit' => [
            // Limit access to get item operation only if the logged user is one of:
            // - have ROLE_ADMIN
            'security' => ' is_granted("ROLE_STATS")',
            'method'   => 'GET',
            'path' => '/stats/numberOfVisits',
            'controller'=> NbVisitController::class,
        ],
    ],
    itemOperations: [
        'get' => [
            'security' => ' is_granted("ROLE_STATS")',
            'method'   => 'GET',
            
        ],
    ]
)]
class Visit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

 
}
