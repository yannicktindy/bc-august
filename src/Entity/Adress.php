<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AdressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AdressRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: [
        "get" => ["security" => "is_granted('ROLE_STATS')"],
        ]
)]
class Adress
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(length: 80)]
    #[
        Assert\NotBlank([
            'message' => 'Le nom de l\'adresse est obligatoire',
        ]),
        Assert\Length([
            'min' => 2,
            'max' => 80,
            'minMessage' => 'Le nom de l\'adresse doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de l\'adresse doit faire au maximum {{ limit }} caractères',
        ]),
    ]
    private ?string $line1 = null;

    #[ORM\Column(length: 80, nullable: true)]
    #[
        Assert\Length([
            'min' => 2,
            'max' => 80,
            'minMessage' => 'Le nom de l\'adresse doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de l\'adresse doit faire au maximum {{ limit }} caractères',
        ]),
    ]
    private ?string $line2 = null;


    #[ORM\Column(length: 80, nullable: true)]
    #[
        Assert\Length([
            'min' => 2,
            'max' => 80,
            'minMessage' => 'Le nom de l\'adresse doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de l\'adresse doit faire au maximum {{ limit }} caractères',
        ]),
    ]
    private ?string $line3 = null;

    #[ORM\ManyToOne(inversedBy: 'adresses')]
    private ?User $user = null;

    #[ORM\Column(length: 20)]
    private ?string $zip = null;
    #[
        Assert\NotBlank([
            'message' => 'Le ZIP est obligatoire',
        ]),
        Assert\Length([
            'min' => 2,
            'max' => 20,
            'minMessage' => 'Le nom de l\'adresse doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de l\'adresse doit faire au maximum {{ limit }} caractères',
        ]),
    ]

    #[ORM\Column(length: 80)]
    #[
        Assert\NotBlank([
            'message' => 'Le nom de l\'adresse est obligatoire',
        ]),
        Assert\Length([
            'min' => 2,
            'max' => 80,
            'minMessage' => 'Le nom de l\'adresse doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de l\'adresse doit faire au maximum {{ limit }} caractères',
        ]),
    ]
    private ?string $city = null;

    #[ORM\OneToMany(mappedBy: 'adress', targetEntity: Basket::class)]
    private Collection $baskets;

    public function __construct()
    {
        $this->baskets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLine1(): ?string
    {
        return $this->line1;
    }

    public function setLine1(string $line1): self
    {
        $this->line1 = $line1;

        return $this;
    }

    public function getLine2(): ?string
    {
        return $this->line2;
    }

    public function setLine2(?string $line2): self
    {
        $this->line2 = $line2;

        return $this;
    }

    public function getLine3(): ?string
    {
        return $this->line3;
    }

    public function setLine3(?string $line3): self
    {
        $this->line3 = $line3;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection<int, Basket>
     */
    public function getBaskets(): Collection
    {
        return $this->baskets;
    }

    public function addBasket(Basket $basket): self
    {
        if (!$this->baskets->contains($basket)) {
            $this->baskets[] = $basket;
            $basket->setAdress($this);
        }

        return $this;
    }

    public function removeBasket(Basket $basket): self
    {
        if ($this->baskets->removeElement($basket)) {
            // set the owning side to null (unless already changed)
            if ($basket->getAdress() === $this) {
                $basket->setAdress(null);
            }
        }

        return $this;
    }
}
