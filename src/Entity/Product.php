<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("ROLE_STATS")'],
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("ROLE_STATS")' ],
    ],
)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(length: 40)]
    #[
        Assert\NotBlank,
        Assert\Length([
            'min' => 2,
            'max' => 40,
            'minMessage' => 'Le nom du produit doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom du produit doit faire au maximum {{ limit }} caractères',
        ]),
    ]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[
        Assert\NotBlank,
        Assert\Length([
            'min' => 2,
            'max' => 255,
            'minMessage' => 'Le nom de la description doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de la description doit faire au maximum {{ limit }} caractères',
        ]),

    ]
    private ?string $description = null;

    #[ORM\Column(length: 40)]
    #[
        Assert\NotBlank,
        Assert\Length([
            'min' => 2,
            'max' => 40,
            'minMessage' => 'Le nom de la reference doit faire au moins {{ limit }} caractères',
            'maxMessage' => 'Le nom de la reference doit faire au maximum {{ limit }} caractères',
        ]),

    ]
    private ?string $reference = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $entryDate = null;

    #[ORM\Column]
    private ?float $tva = null;

    #[ORM\Column]

    private ?bool $isPromo = false;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Picture::class)]
    private Collection $pictures;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?Brand $brand = null;



    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Comment::class)]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Line::class)]
    private Collection $bLine;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    #[
        Assert\NotBlank,
        Assert\GreaterThanOrEqual(0),
        Assert\LessThanOrEqual(700),
        Assert\Type(type: 'numeric'),
    ]
    private ?string $price = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 4, scale: 2, nullable: true)]
    private ?string $note = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?Animal $animal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $picture = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isNew = null;

   




    public function __construct()
    {
        $this->pictures = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->bLine = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entryDate;
    }

    public function setEntryDate(\DateTimeInterface $entryDate): self
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function setTva(float $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function isIsPromo(): ?bool
    {
        return $this->isPromo;
    }

    public function setIsPromo(bool $isPromo): self
    {
        $this->isPromo = $isPromo;

        return $this;
    }

    /**
     * @return Collection<int, Picture>
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setProduct($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getProduct() === $this) {
                $picture->setProduct(null);
            }
        }

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setProduct($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getProduct() === $this) {
                $comment->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Line>
     */
    public function getBLine(): Collection
    {
        return $this->bLine;
    }

    public function addBLine(Line $bLine): self
    {
        if (!$this->bLine->contains($bLine)) {
            $this->bLine[] = $bLine;
            $bLine->setProduct($this);
        }

        return $this;
    }

    public function removeBLine(Line $bLine): self
    {
        if ($this->bLine->removeElement($bLine)) {
            // set the owning side to null (unless already changed)
            if ($bLine->getProduct() === $this) {
                $bLine->setProduct(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {   


        $this->note = $note;

        return $this;

    }

    public function getAnimal(): ?Animal
    {
        return $this->animal;
    }

    public function setAnimal(?Animal $animal): self
    {
        $this->animal = $animal;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function isIsNew(): ?bool
    {
        return $this->isNew;
    }

    public function setIsNew(?bool $isNew): self
    {
        $this->isNew = $isNew;

        return $this;
    }





}
