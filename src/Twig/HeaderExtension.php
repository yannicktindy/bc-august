<?php

namespace App\Twig;

use App\Repository\AnimalRepository;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class HeaderExtension extends AbstractExtension
{

    public function __construct(
        private AnimalRepository $animalRepository,
        private CategoryRepository $categoryRepository,
        private ProductRepository $productRepository
    ) { }


    public function getFunctions(): array
    {
        return [
            new TwigFunction('functionTwigGetAnimals', [$this, 'getAnimals']),
            new TwigFunction('functionTwigGetCategories', [$this, 'getCategories']),
            new twigFunction('functionTwigGetQantity', [$this, 'getQuantity']),
        ];
    }

    //Fonction pour récuperer tous les animeaux
    public function getAnimals(): array
    {
        return $this->animalRepository->findAll();

    }

    public function getCategories(): array
    {
        return $this->categoryRepository->findAll();
        
    }

    public function getQuantity(SessionInterface $session): int
    {
        $panier = $session->get('panier', []);
        $total = 0;
        foreach ($panier as $id => $quantity) {
            
            $total += $quantity; 
        }

        return $total;   
    }









    
}
