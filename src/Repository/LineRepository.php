<?php

namespace App\Repository;

use App\Entity\Line;
use App\Entity\Basket;
use DateTime;
use App\Enum\BasketStatusEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Line>
 *
 * @method Line|null find($id, $lockMode = null, $lockVersion = null)
 * @method Line|null findOneBy(array $criteria, array $orderBy = null)
 * @method Line[]    findAll()
 * @method Line[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private BasketRepository $basketRepository,
        )

    {
        parent::__construct($registry, Line::class);
    }

    public function add(Line $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Line $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * sum  of lines by pice and quantity where basket status = BasketStatusEnum::STATUS_VALIDATED
     */
    public function getSumLinesByBasketStatus(?\DateTime $startDate = null, ?\DateTime $endDate = null): float
    {
        if ($startDate === null || $endDate === null) {
            $endDate = new DateTime('now');
            $startDate = new DateTime('2020-01-01');
        }

            return $this->createQueryBuilder('line')
                ->select('SUM(line.price * line.quantity)')
                ->leftJoin('line.basket', 'basket')
                ->where('basket.status = :status')
                ->setParameter('status', BasketStatusEnum::STATUS_VALIDATED)
                // ->andWhere('line.basket.dateCreation BETWEEN :startDate AND :endDate')
                // ->setParameter('startDate', $startDate)
                // ->setParameter('endDate', $endDate)
                ->getQuery()
                ->getSingleScalarResult();
      

        // return $this->createQueryBuilder('line')
        //     ->select('SUM(line.quantity * line.price)', 'basket')
        //     ->join('line.basket', 'basket')
        //     ->where('line.basket.status = :status')
        //     ->setParameter('status', BasketStatusEnum::STATUS_VALIDATED)

        //     ->getQuery()
        //     ->getSingleScalarResult();
    }





//    /**
//     * @return Line[] Returns an array of Line objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Line
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
