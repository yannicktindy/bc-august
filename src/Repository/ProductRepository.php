<?php

namespace App\Repository;

use App\Entity\Product;
use APP\Entity\Line;
use APP\Entity\Basket;
use App\Enum\BasketStatusEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Faker\Core\DateTime;
use Doctrine\ORM\QueryBuilder;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends AbstractLanimalerieRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // findProductsByCategory($category)
    public function findProductByCategory($category)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.category = :category')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult();
    }

    // find products by best notes()
    public function findProductsByBestValue()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->join('p.comments', 'c')
            ->where('c.value > 4') // on veut les produits qui ont une note supérieure à 4
            ->getQuery()
            ->getResult();

    }

    // find Products By Promo()
    public function findProductsByPromo()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.isPromo == true') // on veut les produits qui sont en promo
            ->getQuery()
            ->getResult();
    }

    // find new products by date
    public function findNewProductsByDate()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.createdAt > :date')
            ->setParameter('date', new \DateTime('-1 month')) // on veut les produits qui sont créés depuis 1 mois
            ->getQuery()
            ->getResult();
    }



    /**
     *  product count the number of lines and sort by number of lines descending
     */
    public function getBestSellers(?\DateTime $startDate = null, ?\DateTime $endDate = null): array
    {
        if ($startDate === null || $endDate === null) {
            $endDate = new DateTime('now');
            $startDate = new DateTime('2020-01-01');
        }
        $qb = $this->createQueryBuilder('p');
        $qb->select('p.name as product', 'COUNT(line.quantity) as countProduct')
            ->join('p.lines', 'line')
            ->join('line.basket', 'basket')
            ->where('basket.status = :status')
            ->setParameter('status', BasketStatusEnum::STATUS_VALIDATED)
            ->andWhere('basket.dateCreation BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->groupBy('p.id')
            ->orderBy('countProduct', 'DESC')
            ->setMaxResults(10);
            $query = $qb->getQuery();
            $bestSellers = $query->getResult();
            return $bestSellers;
            ;
    }      
    
    /**
     *  findAll procucts by animal id
     */
    public function findAllByAnimalId(int $id): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.animal = :animal')
            ->setParameter('animal', $id)
            ->getQuery()
            ->getResult();
        
    }

    public function getQbAll(): QueryBuilder
    {
        $qb = parent::getQbAll();
        return $qb->select('product','brand', 'animal')
        ->join('product.brand', 'brand')
        ->join('product.animal', 'animal')
        ->orderBy('product.name');
           
    }

    public function getQbAllWithAnimalId($id): QueryBuilder
    {
        $qb = parent::getQbAll();
        return $qb->select('product', 'animal')
        ->join('product.animal', 'animal')
        ->where('animal.id = :id')
        ->setParameter('id', $id)
        ->orderBy('product.name');
           
    }

    public function getQbAllWithCategoryId($id): QueryBuilder
    {
        $qb = parent::getQbAll();
        return $qb->select('product', 'animal', 'category')
        ->leftJoin('product.animal', 'animal')
        ->leftJoin('animal.categories', 'category')
        ->where('category.id = :id')
        ->setParameter('id', $id)
        ->orderBy('product.name');
    }

    public function gateProductNote(): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p.name as product', 'AVG(c.value) as note')
            ->join('p.comments', 'c')
            ->groupBy('p.id')
            ->orderBy('note', 'DESC')
            ->setMaxResults(10);
            $query = $qb->getQuery();
            $bestSellers = $query->getResult();
            return $bestSellers;
    }

    // find product by note
    public function findBest(): array {
        return $this->createQueryBuilder('product')
            ->select('product')
            ->having('product > 4')
            ->groupBy('product.id')
            ->orderBy('product.note', 'DESC')
            ->getQuery()
            ->getResult();
    }



    //recuperer les infos pour le paginator
    // public function getQbAll(): QueryBuilder
    // {
    //     // $qb = parent::getQbAll();
    //     return $qb->select('product', 'animal', 'category', 'brand')
    //         ->join('product.animal', 'animal')
    //         ->join('product.category', 'category')
    //         ->join('product.brand', 'brand')          
    //         ->orderBy('product.id', 'ASC');
    // }

    // public function getQbAll(): QueryBuilder
    // {
    //     $qb = parent::getQbAll();
    //     return $qb->select('produit','promotion', 'marque', 'categorie','photo')
    //         ->leftJoin('produit.promotion', 'promotion')
    //         ->leftJoin('produit.marque', 'marque')
    //         ->leftJoin('produit.categorie', 'categorie')
    //         ->leftJoin('produit.photo', 'photo')
    //         ->orderBy('produit.libelle', 'ASC')
    //         ;
    // }

    /**
     *  findAll procucts by category id
     */
    // public function findAllByCategoryId(int $id): array
    // {
    //     return $this->createQueryBuilder('p')
    //         ->join('p.animal', 'a')
    //         ->join('a.category', 'c')
    //         ->where('c.id = :category')
    //         ->setParameter('category', $id)
    //         ->getQuery()
    //         ->getResult();
        
    // }

    // SELECT product.name as product, COUNT(line.quantity) as countProduct
    // FROM product
    // JOIN line ON product.id = line.product_id
    // JOIN basket ON line.basket_id = basket.id
    // WHERE basket.status = 300
    // WHERE basket.dateCreation Between '2020-01-01' AND '2020-01-31'
    // ORDER BY countProduct DESC;
    // Limit 10
    











    // ->join('p.lines', 'line')
    //         ->groupBy('countProduct')
    //         ->orderBy('p', 'DESC') 
    //         ->setMaxResults('10')
    //         ->getQuery()
    //         ->getResult();

            
    // find products most sold join line DESC
    // public function getBestSellers()
    // {
    //     return $this->createQueryBuilder('p')
    //         ->select('count(p)', 'p.name', 'p.price')
    //         ->join('p.lines', 'l')
    //         ->groupBy('p.name')
    //         ->orderBy('count(p)', 'DESC')
    //         ->getQuery()
    //         ->getResult();
            
    // }


//    /**
//     * @return Product[] Returns an array of Product objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Product
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
