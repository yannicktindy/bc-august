<?php

namespace App\Repository;

use App\Entity\Basket;
use App\Entity\Line;
use App\Enum\BasketStatusEnum;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;



/**
 * @extends ServiceEntityRepository<Basket>
 *
 * @method Basket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Basket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Basket[]    findAll()
 * @method Basket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasketRepository extends AbstractLanimalerieRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Basket::class);
    }

    public function add(Basket $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Basket $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * count the number of baskets between two dates and a status of basket
     */
    public function getNumberBasketsByStatus(?\DateTime $startDate = null, ?\DateTime $endDate = null): int 
    {
        if ($startDate === null || $endDate === null) {
            $endDate = new DateTime('now');
            $startDate = new DateTime('2020-01-01');
        }
        $qb = $this->createQueryBuilder('basket');
        $qb->select('count(basket.id)')
            ->where('basket.status = :status')
            ->setParameter('status', BasketStatusEnum::STATUS_CANCELED)
            ->andWhere('basket.dateCreation BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);
        $query = $qb->getQuery();
        $nbBasket = $query->getSingleScalarResult();
        return $nbBasket;
    }

        /**
     * count the number of order between two dates and a status of basket
     */
    public function getNumberOrderByStatus(?\DateTime $startDate = null, ?\DateTime $endDate = null): int 
    {
        if ($startDate === null || $endDate === null) {
            $endDate = new DateTime('now');
            $startDate = new DateTime('2020-01-01');
        }
        $qb = $this->createQueryBuilder('basket');
        $qb->select('count(basket.id)')
            ->where('basket.status = :status')
            ->setParameter('status', BasketStatusEnum::STATUS_VALIDATED)
            ->andWhere('basket.dateCreation BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);
        $query = $qb->getQuery();
        $nbBasket = $query->getSingleScalarResult();
        return $nbBasket;
    }

    public function getQbAll(): QueryBuilder
    {
        $qb = parent::getQbAll();
        return $qb->select('basket', 'line', 'product', 'user', 'adress', 'payment')
            ->join('basket.bLine', 'line')
            ->join('line.product', 'product')
            ->join('basket.user', 'user')
            ->join('basket.adress', 'adress')
            ->join('basket.payment', 'payment')
            ->orderBy('basket.dateCreation', 'DESC');

    }

    

    // public function sumOfSold(): float
    // {
    //     $allOrdersLines = $this->getBLine();
    //     $sum = 0;
    //     foreach ($allOrdersLines as $orderLine) {
    //         $sum += $orderLine.price * $orderLine.quantity;
    //     }
    //     return round($sum, 2);
    // }    


    //     return $this->createQueryBuilder('basket')
    //         ->select('SUM(bline.price * bline.quantity)')
    //         ->join('basket.bline', 'line')
    //         ->where('basket.status = :status')
    //         ->setParameter('status', BasketStatusEnum::STATUS_VALIDATED)
    //         ->getQuery()
    //         ->getSingleScalarResult();
    // }

    

    // /**
    //  *  amount of all baskets join lines
    //  */
    // public function getAmountBaskets(): int
    // {
    //     $qb = $this->createQueryBuilder('basket');
    //     $qb->select('SUM(basket.lines.price * basket.lines.quantity)'); /
    //         ->join('basket.lines', 'lines');
    //         ->where('basket.status = :status')
    //         ->setParameter('status', BasketStatusEnum::STATUS_VALIDATED);
    //     $query = $qb->getQuery();
    //     $amount = $query->getSingleScalarResult();
    //     return $amount;
    // }





    // /**
    //  * count the number of baskets between two dates and a status of basket
    //  */
    // public function countBasketBetweenDates(\DateTime $startDate = null, \DateTime $endDate = new DateTime()): int {
        
    //     $qb = $this->createQueryBuilder('basket');
    //     $qb->select('count(basket.id)')
    //         ->where('basket.status = :status')
    //         ->setParameter('status', Enum::STATUS_CREATED)
    //         ->andWhere('basket.createdAt BETWEEN :startDate AND :endDate')
    //         ->setParameter('startDate', $startDate)
    //         ->setParameter('endDate', $endDate);
    //     $query = $qb->getQuery();
    //     $nbBasket = $query->getSingleScalarResult();
    //     return $nbBasket;
    // }









//    public function findOneBySomeField($value): ?Basket
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    // public function getNumberBasketsByStatusAndDate(DateTime $startDate, DateTime $endDate): int
    // {
    //     $qb = $this->createQueryBuilder('basket');
    //     $qb->select('count(basket.id)')
    //     ->andWhere('basket.status = :status')
    //     ->andWhere('basket.date BETWEEN :startDate AND :endDate')
    //     ->setParameter('status', BasketStatusEnum::STATUS_CANCELED)
    //     ->setParameter('startDate', $startDate)
    //     ->setParameter('endDate', $endDate);
    //     $query = $qb->getQuery();
    //     $nbBaskets = $query->getSingleScalarResult();
    //     return $nbBaskets;
    // }
}
