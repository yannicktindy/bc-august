<?php

namespace App\Repository;

use App\Entity\Animal;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @extends ServiceEntityRepository<Animal>
 *
 * @method Animal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Animal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Animal[]    findAll()
 * @method Animal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimalRepository extends AbstractLanimalerieRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Animal::class);
    }

    public function add(Animal $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Animal $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getQbAll(): QueryBuilder
    {
        $qb = parent::getQbAll();
        return $qb->select('animal', 'product', 'category')
        ->join('animal.product', 'product')
        ->join('animal.categories', 'category')
        ->orderBy('animal.name', 'ASC');
    }

    public function getQbAllByCategory($id): QueryBuilder
    {
        $qb = parent::getQbAll();
        return $qb->select('animal', 'product', 'category')
        ->join('animal.product', 'product')
        ->join('animal.categories', 'category')
        ->where('category.id = :id')
        ->setParameter('id', $id)
        ->orderBy('animal.name', 'ASC');
    }

    // public function findAnimalsAndCategories(): array
    // {
    //     return $this->createQueryBuilder('animal')
    //         ->select('animal','category')
    //         ->join('animal.category', 'category')
    //         ->groupBy('animal.id')
    //         ->getQuery()
    //         ->getResult();

    // }





//    /**
//     * @return Animal[] Returns an array of Animal objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Animal
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
