<?php

namespace App\Repository;

use App\Entity\Statistic;
use DateTime;
use App\Enum\BasketStatusEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\BasketRepository;
use App\Repository\LineRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Repository\VisitRepository;

/**
 * @extends ServiceEntityRepository<Statistic>
 *
 * @method Statistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistic[]    findAll()
 * @method Statistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private BasketRepository $basketRepository,
        private VisitRepository $visitRepository,
        private UserRepository  $userRepository,
        private ProductRepository $productRepository,
        private LineRepository  $lineRepository,
        )
    {
        parent::__construct($registry, Statistic::class);
    }


    /**
     *  create a new statistic between two dates
     */
    public function createStatistic(?\DateTime $startDate = null, ?\DateTime $endDate = null) {
        if ($startDate === null || $endDate === null) {
            $endDate = new DateTime('now');
            $startDate = new DateTime('2020-01-01');
        }
        
        $statistic = new Statistic();
        $statistic->setStartDate($startDate);
        $statistic->setEndDate($endDate);
        $nbVisit = $this->visitRepository->getNumberVisit($startDate, $endDate);
        $statistic->setNbVisit($nbVisit);
        $nbBasket = $this->basketRepository->getNumberBasketsByStatus($startDate, $endDate);
        $statistic->setNbBasket($nbBasket);
        $nbOrder = $this->basketRepository->getNumberOrderByStatus($startDate, $endDate);
        $statistic->setNbOrder($nbOrder);
        $totalSum = round($this->lineRepository->getSumLinesByBasketStatus(), 2);
        $statistic->setTotalSum($totalSum);

    }

    public function add(Statistic $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Statistic $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Statistic[] Returns an array of Statistic objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Statistic
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
