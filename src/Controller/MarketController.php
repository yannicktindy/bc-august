<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Entity\Line;
use App\entity\Product;
use App\Enum\BasketStatusEnum;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class MarketController extends AbstractController
{
    #[Route('/market', name: 'app_market')]
    public function index(Request $request, SessionInterface $session, ProductRepository $productRepository): Response
    {
        $panier = $session->get('panier', []);
        $panierWithData = [];
        foreach ($panier as $id => $quantity) {
            
            $panierWithData[] = [
                'product' => $productRepository->find($id),
                'quantity' => $quantity,
            ];
        }
        $total = 0;
        foreach ($panierWithData as $item) {
            $totalItem = $item['product']->getPrice() * $item['quantity'];
            $total += $totalItem;
        }

        // return $this->redirect($request->headers->get('referer'));

        return $this->render('market/index.html.twig', [
            'items' => $panierWithData,
            'total' => $total,
        ]);
        
        
    }

    #[Route('/market/add/{id}', name: 'app_market_add')]
    public function add ($id, SessionInterface $session)
    {
        
        $panier = $session->get('panier', []);
        if (!empty($panier[$id])) {
            $panier[$id]++;
        } else {
            $panier[$id] = 1;
        }
        $panier[$id] = 1;
        $session->set('panier', $panier);

        return $this->redirectToRoute("app_market");
    }  

    #[Route('/market/remove/{id}', name: 'app_market_remove')]
    public function remove ($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);

        if (!empty($panier[$id])) {
            unset($panier[$id]);
        }
        $session->set('panier', $panier);

        return $this->redirectToRoute("app_market");
    }
 
    #[Route('/market/validation', name: 'app_market_validation')]
    public function validation (SessionInterface $session, EntityManagerInterface $entityManagerInterface, ProductRepository $productRepository)
    {
        $panier = $session->get('panier', []);

        $basket = new Basket();
        $basket->setDateCreation(new \DateTime('now'));
        $basket->setStatus(BasketStatusEnum::STATUS_CREATED);
        // if ($app.user) {
        //     $basket->setUser(app.user);
        // }
        $basket->setUser($this->getUser());
        $entityManagerInterface->persist($basket);

        foreach ($panier as $id => $quantity) {
            $line = new Line();
            $line->setBasket($basket);
            $line->setProduct($productRepository->find($id));
            $line->setQuantity($quantity);
            $line->setPrice($productRepository->find($id)->getPrice());
            $entityManagerInterface->persist($line);
        }

        $entityManagerInterface->flush();

        return $this->redirectToRoute("app_home");

    }

}
