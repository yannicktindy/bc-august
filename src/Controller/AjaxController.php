<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Enum\BasketStatusEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/ajax')]
class AjaxController extends AbstractController
{

    public static string $CART = 'CART';
    public static string $QTY = 'QTY';

    #[Route('/addItemToCart/{datas}', name: 'ajax_add')]
    
//     public function __construct(        
//         private EntityManager $entityManager,
//       )
//   {}

    
    public function index(
        Request $request,
        SessionInterface $session,
        EntityManagerInterface  $entityManager,
    ): Response
    {
        // create a new basket if not exist
        // if (!$session->has(Basket()::$panier)) {
        //     $panier = new Basket();

        //     $panier->setCreatedAt(new \DateTime());
        //     $panier->setStatus(BasketStatusEnum::CREATED);
            
        //     $this->entityManager->persist($panier);
        //     $this->entityManager->flush();

        //     $session->set('panier', $panier);
        // } else {
            
        // }
        
        // On récupère les données envoyées en fetch dans le fichier ts
        $datas = json_decode($request->get('datas'), true);

        // On crée un tableau vide pour stocker les données du produit dans la session
        $currentSession = [];

        // Si la session existe
        if ($session->has(self::$CART)) {
            // On récupère les données de la session
            $currentSession = $session->get(self::$CART);
        }

        // Si le produit n'existe pas dans la session
        if (!isset($currentSession[$datas['produitId']])) {
            // On ajoute le produit et la quantité dans le tableau
            $currentSession[$datas['produitId']] = $datas['qty'];
        } else {
            // Sinon on ajoute la quantité au produit existant
            $currentSession[$datas['produitId']] += $datas['qty'];
        }

        // On enregistre le tableau CART dans la session
        $session->set(self::$CART, $currentSession);

        // On crée une variable pour stocker la quantité totale
        $qtyTotal = 0;

        // On parcourt le tableau pour récupérer la quantité totale
        foreach ($currentSession as $item) {
            $qtyTotal += $item;
        }

        // On enregistre la quantité totale dans la session
        $session->set(self::$QTY, $qtyTotal);

        // On retourne vers le ts un json avec la quantité totale pour l'affichage dans le panier
        return new JsonResponse(['qtyTotale' => $qtyTotal]);
    }

    /**
     * @throws Exception
     */
}    
