<?php

namespace App\Controller;

use App\Entity\Visit;
use App\Entity\Animal;
use App\Entity\Comment;
use App\Repository\AnimalRepository;
use App\Repository\BasketRepository;
use App\Repository\CommentRepository;
use App\Repository\ProductRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(
        private BasketRepository $basketRepository,
        private VisitRepository $visitRepository,
        private AnimalRepository $animalRepository,
        private ProductRepository $productRepository,
        private EntityManagerInterface $entityManager,
        private CommentRepository $commentRepository
        )
    {
       
    }

    #[Route('/', name: 'app_home')]
    public function index(ProductRepository $productRepository): Response
    {


        $visit = new Visit();
        $visit->setDate(new \DateTime());
        $this->entityManager->persist($visit);
        $this->entityManager->flush();

        $products = $this->productRepository->findAll();
        // find product by best note
            foreach($products as $product) {
                $comments = $product->getComments();
                $note = 0;
                $date= $product->getEntryDate();
                if ($date > new \DateTime('-1 month')) {
                  $product->setIsNew(true);  
                   
                } else {
                    $product->setIsNew(false);
                }

                foreach($comments as $comment) {
                    $note += $comment->getValue();
                }
                if (count($comments) > 0) {
                    $note = $note / count($comments);
                    $product->setNote($note / count($comments));
                } else {
                    $product->setNote(0);

                }
                ;
                $this->entityManager->persist($product);
            }
            $this->entityManager->flush();    

        // $Categories= $this->animalRepository->findCategories();
        // $animals = $this->animalRepository->findAll();
        // $categories = $this->animalRepository->findAnimalsCategories();
        // dump($categories);
               
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            // 'animals' => $this->animalRepository->findProdCategByAnimalName(),
            //'best' => $productRepository->findBy(['note > 4'],[], 4),
            // 'mostPlayedGames' => $gameRepository->getGroupedByGameByOrder(),
            // 'lastPublished' => $gameRepository->findBy([], ['publishedAt' => 'DESC'], 4),
            // 'lastComments' => $commentRepository->findBy([], ['createdAt' => 'DESC'], 4),
            // 'mostSellers' => $gameRepository->getGroupedByGameByOrder('COUNT(library.game)'),
            // 'genres' => $genreRepository->findBy([], ['name' => 'ASC'], 9),
        ]);
    }

    #[Route('/show/{id}', name: 'app_home_show')]
    public function show($id): Response
    {

        $product = $this->productRepository->find($id);
        
            // dump($product);   
        return $this->render('home/prod.html.twig', [
            'controller_name' => 'HomeController',
            'product' => $product,
            // 'mostPlayedGames' => $gameRepository->getGroupedByGameByOrder(),
            // 'lastPublished' => $gameRepository->findBy([], ['publishedAt' => 'DESC'], 4),
            // 'lastComments' => $commentRepository->findBy([], ['createdAt' => 'DESC'], 4),
            // 'mostSellers' => $gameRepository->getGroupedByGameByOrder('COUNT(library.game)'),
            // 'genres' => $genreRepository->findBy([], ['name' => 'ASC'], 9),
            // find products where isPromo = true
            'promoProducts' => $this->productRepository->findBy(['isPromo' => true], ['id' => 'DESC'], 4),
            // fiind last products
            'lastProducts' => $this->productRepository->findBy([], ['id' => 'DESC'], 4),

        ]);
    }

   




}