<?php

namespace App\Controller;

use App\Repository\AnimalRepository;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontProductController extends AbstractController
{
    public function __construct(
        private AnimalRepository $animalRepository,
        private CategoryRepository $categoryRepository,
        private ProductRepository $productRepository,
        
        
    ) { }

    #[Route('/front/product/animal/{id}', name: 'app_front_product_animal')]
    public function index(
        ProductRepository $productRepository, 
        Request $request, 
        PaginatorInterface $paginator, 
        int $id
        ): Response
    {
        $qb = $productRepository->getQbAllWithAnimalId($id);
        // Paginator
        $products = $paginator->paginate(
            $qb,
            $request->query->getInt('page',1),8
        );
        $animal = $this->animalRepository->find($id);

        return $this->render('front_product/index.html.twig', [
            'products' => $products,
            'animal' => $animal,
        ]);
    }

    #[Route('/front/product/category/{id}', name: 'app_front_product_category')]
    public function productByCategory(
        ProductRepository $productRepository, 
        Request $request, 
        PaginatorInterface $paginator, 
        int $id
        ): Response
    {
        $qb = $productRepository->getQbAllWithCategoryId($id);
        // Paginator
        $products = $paginator->paginate(
            $qb,
            $request->query->getInt('page',1),8
        );
        $category = $this->categoryRepository->find($id);

        return $this->render('front_product/category.html.twig', [
            'products' => $products,
            'category' => $category,
        ]);
    }
}

