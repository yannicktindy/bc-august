<?php

namespace App\Controller;

use App\Repository\AnimalRepository;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use ContainerGddlKN8\getLexikFormFilter_Type_FilterTextService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontAnimalController extends AbstractController
{

    public function __construct(
        private AnimalRepository $animalRepository,
        private ProductRepository   $productRepository,
        private CategoryRepository $categoryRepository,
        
    ) { }
    #[Route('/front/animal/{id}', name: 'app_front_animal')]
    public function showProductsByAnimalId(int $id,): Response
    {
        $note=0;
        $products = $this->productRepository->findAll();
        foreach ($products as $product) {
            $comments = $product->getComments();
            if(count($comments) > 0) {
                $nbComments = count($comments);
                $sumNotes=0;
                for($i = 0; $i < $nbComments; $i++) {
                $sumNotes += $comments[$i]->getValue();    
                }
            $note = $sumNotes / $nbComments;
            $product->setNote($note);
            }
        }
        /**
         *  findAll procucts by animal id
         */
        $products = $this->productRepository->findAllByAnimalId($id);
        /**
         *  find animal by id
         */
        $animal = $this->animalRepository->find($id);
        
        return $this->render('front_animal/index.html.twig', [
            'controller_name' => 'FrontAnimalController',
            'products' => $products,
            'animal' => $animal,
            'note' => $note,
        ]);
    }
}

