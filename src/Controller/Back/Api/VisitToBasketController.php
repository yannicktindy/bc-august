<?php

namespace App\Controller\Back\Api;

use App\Repository\BasketRepository;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Repository\VisitRepository;

class VisitToBasketController extends AbstractController
{

    public function __construct(
        EntityManagerInterface $entityManager,
        private VisitRepository $visitRepository,
        private BasketRepository $basketRepository,
        )
        
    {
        $this->entityManager = $entityManager;
    }


    public function __invoke(): JsonResponse
    {
  

        $nbVisit = $this->visitRepository->getNumberVisit();
        $nbBasket = $this->basketRepository->getNumberBasketsByStatus();
        $visitToBasket = round((100 / $nbVisit) * $nbBasket, 2);
        return new JsonResponse(['ConversionVisitToBasket' => $visitToBasket]);

    }
}