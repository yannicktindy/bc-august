<?php

namespace App\Controller\Back\Api;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\BasketRepository;
use App\Repository\LineRepository;


class TotalSoldController extends AbstractController
{

    public function __construct(
        EntityManagerInterface $entityManager,
        private LineRepository $lineRepository
        )
    {
        $this->entityManager = $entityManager;
    }


    public function __invoke(): JsonResponse
    {
  

        $totalSold = $this->lineRepository->getSumLinesByBasketStatus();

        return new JsonResponse(['TotalSold' => $totalSold]);

    }
}