<?php

namespace App\Controller\Back\Api;

use App\Repository\BasketRepository;
use App\Repository\LineRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Repository\VisitRepository;

class AverageOrderController extends AbstractController
{

    public function __construct(
        EntityManagerInterface $entityManager,
        
        private BasketRepository $basketRepository,
        private LineRepository $lineRepository,
        )
       
    {
        $this->entityManager = $entityManager;
    }


    public function __invoke(): JsonResponse
    {

        $totalSold = $this->lineRepository->getSumLinesByBasketStatus();
        $nbOrder = $this->basketRepository->getNumberOrderByStatus();
        $averageOrder = round($totalSold / $nbOrder, 2);
        return new JsonResponse(['AverageOrder' => $averageOrder]);

    }
}