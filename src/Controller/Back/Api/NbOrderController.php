<?php

namespace App\Controller\Back\Api;

use App\Repository\BasketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Repository\VisitRepository;

class NbOrderController extends AbstractController
{

    public function __construct(
        EntityManagerInterface $entityManager,
        private BasketRepository $basketRepository
        )
    {
        $this->entityManager = $entityManager;
    }


    public function __invoke(): JsonResponse
    {
  

        $nbOrder = $this->basketRepository->getNumberOrderByStatus();

        return new JsonResponse(['NumberOrder' => $nbOrder]);

    }
}