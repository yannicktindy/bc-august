<?php

namespace App\Controller\Back\Api;

use App\Repository\StatisticRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Repository\VisitRepository;

class AllStatisticController extends AbstractController
{

    public function __construct(
        private StatisticRepository $statisticRepository,
        EntityManagerInterface $entityManager,

        )
        
    {
        $this->entityManager = $entityManager;
    }


    public function __invoke(): JsonResponse
    {
        // $statistic = $this->statisticRepository->createStatistic(?\DateTime $startDate = null, ?\DateTime $endDate = null);
        $statistic = $this->statisticRepository->createStatistic();

        return new JsonResponse(['statistic' => $statistic]);

    }
}