<?php

namespace App\Controller\Back\Api;

use App\Repository\BasketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Repository\VisitRepository;

class CanceledBasketController extends AbstractController
{

    public function __construct(
        EntityManagerInterface $entityManager,
        VisitRepository $visitRepository,
        private BasketRepository $basketRepository,
        )
        
    {
        $this->entityManager = $entityManager;
    }


    public function __invoke(): JsonResponse
    {

        $nbBasket = $this->basketRepository->getNumberBasketsByStatus();
        $nbOrder = $this->basketRepository->getNumberOrderByStatus();
        $basketscanceled = round((100 / ($nbBasket + $nbOrder)) * $nbBasket, 2);
        return new JsonResponse(['CanceledBasket' => $basketscanceled]);

    }
}