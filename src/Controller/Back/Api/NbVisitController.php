<?php

namespace App\Controller\Back\Api;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Repository\VisitRepository;

class NbVisitController extends AbstractController
{

    public function __construct(
        private VisitRepository $visitRepository
        )
    {
    }


    public function __invoke(): JsonResponse
    {
          $nbVisit = $this->visitRepository->getNumberVisit();
        dump($nbVisit);
        return new JsonResponse(['NumberVisit' => $nbVisit]);
    }
}