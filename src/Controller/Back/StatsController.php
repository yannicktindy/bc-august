<?php

namespace App\Controller\Back;


use App\Repository\BasketRepository;
use App\Repository\LineRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Repository\VisitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin')]
class StatsController extends AbstractController
{
    public function __construct(
        private BasketRepository $basketRepository,
        private VisitRepository $visitRepository,
        private UserRepository  $userRepository,
        private ProductRepository $productRepository,
        private LineRepository  $lineRepository
        )
        
    {
       
    }



    #[Route('/stats', name: 'app_admin_stats')]
    public function stats(): Response
    {

        /**
         *  find number of visit
         *  */
        $numberOfVisits = $this->visitRepository->getNumberVisit();
        $numberOfBaskets = $this->basketRepository->getNumberBasketsByStatus();
        $numberOfOrders = $this->basketRepository->getNumberOrderByStatus();

        $convVisitToBasket = round((100 / $numberOfVisits) * $numberOfBaskets, 2);
        $convBasketToOrder = round((100 / ($numberOfBaskets + $numberOfOrders)) * $numberOfOrders, 2);
        $basketscanceled = round((100 / ($numberOfBaskets + $numberOfOrders)) * $numberOfBaskets, 2);

        $totalSum = round($this->lineRepository->getSumLinesByBasketStatus(), 2);
        $averageOrder = round($totalSum / $numberOfOrders, 2);
        $newUsers = $this->userRepository->getNumberUsersWithOneBasket();
        // $bestSellers = $this->productRepository->getBestSellers();
        // dump($bestSellers);
        // $sumOfOrder = $this->basketRepository->sumOfSold();

        // $bestSellers = $this->productRepository->getBestSellers();
        // dump($bestSellers);
        
        return $this->render('back/stats.html.twig', [
            'controller_name' => 'StatsController',
            'nbVisits' => $numberOfVisits,
            'nbBaskets' => $numberOfBaskets,
            'nbOrders' => $numberOfOrders,
            'convVisitToBasket' => $convVisitToBasket,
            'convBasketToOrder' => $convBasketToOrder,
            'basketscanceled' => $basketscanceled,
            'totalSum' => $totalSum,
            'averageOrder' => $averageOrder,
            'newUsers' => $newUsers,
            // 'bestSellers' => $bestSellers,

           
        ]);
    }
}

