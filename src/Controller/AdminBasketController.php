<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Form\BasketType;
use App\Repository\BasketRepository;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/basket')]
class AdminBasketController extends AbstractController
{
    #[Route('/', name: 'app_admin_basket_index', methods: ['GET'])]
    public function index(BasketRepository $basketRepository, Request $request, PaginatorInterface $paginator, FilterBuilderUpdaterInterface $builderUpdater): Response
    {
        $qb = $basketRepository->getQbAll();
        $baskets = $paginator->paginate(
            $qb,
            $request->query->getInt('page',1),10
        );
        return $this->render('admin_basket/index.html.twig', [
            // 'baskets' => $basketRepository->findAll(),
            'baskets' => $baskets,
        ]);
    }

    #[Route('/new', name: 'app_admin_basket_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BasketRepository $basketRepository): Response
    {
        $basket = new Basket();
        $form = $this->createForm(BasketType::class, $basket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $basketRepository->add($basket, true);

            return $this->redirectToRoute('app_admin_basket_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_basket/new.html.twig', [
            'basket' => $basket,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_basket_show', methods: ['GET'])]
    public function show(Basket $basket): Response
    {
        return $this->render('admin_basket/show.html.twig', [
            'basket' => $basket,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_basket_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Basket $basket, BasketRepository $basketRepository): Response
    {
        $form = $this->createForm(BasketType::class, $basket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $basketRepository->add($basket, true);

            return $this->redirectToRoute('app_admin_basket_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_basket/edit.html.twig', [
            'basket' => $basket,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_basket_delete', methods: ['POST'])]
    public function delete(Request $request, Basket $basket, BasketRepository $basketRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$basket->getId(), $request->request->get('_token'))) {
            $basketRepository->remove($basket, true);
        }

        return $this->redirectToRoute('app_admin_basket_index', [], Response::HTTP_SEE_OTHER);
    }
}
