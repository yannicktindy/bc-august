<?php

namespace App\Form;

use App\Entity\Adress;
use App\Entity\Basket;
use App\Entity\Payment;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class BasketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // ->add('status')
            ->add('status', ChoiceType::class, [
                'label' => 'changer le status',
                // 'multiple' => true, //pour pouvoir selectionner plusieurs choix
                // 'expanded' => true, //pour les checkboxs //sans multiple ce sont des radios
                'choices' => [
                    'En préparation' => '400',
                    'Expediée' => '500',
                    'Reçue' => '600',
                    'Remoboursée' => '700',
                ],
            ])
            ->add('dateCreation', DateType::class, [
                
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('datePay', DateType::class, [
                
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('user',EntityType::class , [
                'class' => User::class,
                'choice_label' => 'username',
                'required' => true,
            ])
            ->add('payment',EntityType::class , [
                'class' => Payment::class,
                'choice_label' => 'type',
                'required' => true,
            ])
            ->add('adress',EntityType::class , [
                'class' => Adress::class,
                'choice_label' => 'id',
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Basket::class,
        ]);
    }
}
