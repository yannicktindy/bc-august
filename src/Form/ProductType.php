<?php

namespace App\Form;

use App\Entity\Animal;
use App\Entity\Brand;
use App\Entity\Product;
use App\Entity\Category;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class)
            ->add('picture'
                , FileType::class, [
                'label' => 'Photo',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File(
                        maxSize: '2048k',
                        mimeTypes: ['image/png', 'image/jpeg'],

                    )
                ]
            ]
            )
            ->add('reference')
            ->add('entryDate')
            ->add('tva')
            ->add('isPromo')
            ->add('isNew')
            ->add('price')
            ->add('note')
            ->add('brand',EntityType::class , [
                'class' => Brand::class,
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('animal',EntityType::class , [
                'class' => Animal::class,
                'choice_label' => 'name',
                'required' => true,
            ])
            // ->add('category', EntityFilterType::class, [
            //     'class' => Category::class,
            //     'choice_label' => 'name',
            //     'query_builder' => function (EntityRepository $er) {
            //         return $er->createQueryBuilder('categoriy')
            //             ->orderBy('category.name', 'ASC')
            //             ;
            //     }
            // ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
