Business Case API
Yannick Tindy - https://www.silenus.fr
==================

Crypto tracker API
==================

This is an example of an API used for my web dev training in Human Booster institue. 
It is built with [Symfony](https://symfony.com/) and [API Platform](https://api-platform.com/).

## Requirements

- PHP8.0
- MySQL5.7+ (or mariadb 10.5+)
- [Symfony CLI](https://symfony.com/download)
- [Composer](https://getcomposer.org/)

## Documentation

### basics

- php bin/ is replaced by symfony
- symfony new first --webapp
- symfony serve
- yarn watch
- vider le cache : effacer le dossier cache dans var
- ou en ligne de commande : symfony console cache:clear

#### database and launch
- .env.local
- DATABASE_URL="mysql://root@127.0.0.1:3306/Business"
- symfony console doctrine:database:create
- symfony console make:migration  // créé un dossier migration genere les requette en comparent les BDD et symfony 
- symfony console doctrine:migration:migrate //Integre les modification à la base de donnée

#### entity controller user and auth
- symfony console make:entity
- symfony console make:entity // for relation the typ i relation instead of string
- symfony console debug:router  // to show all the routes
- symfony console make:controller
- symfony console make:user    // create email password et roles[]
- symfony console security:hash:password
- symfony console make:auth    // cree le systeme de connexion
- links : /logout /login /register
- have to redirect connexion on one route line 50 in src/security/appuserauth
- symfony console security:hash:password // or simply - symfony console security 
- repository sont pour realiser les select





### installing 

- [How to install THIS repository on your machine](doc/0-install-repo.md)
- [How to (re)create THIS repository from scratch](doc/1-create-scratch.md)
- [How to create ssl keys](doc/2-ssl-key.md)
- [How to install form on symfony](doc/3-Form.md)
- [How to install yarn sass bootstrap and typescript](doc/4-yarn-sasss-bootsrap-typescript.md)




