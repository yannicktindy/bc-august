#### form
- symfony console make:registration-form
- EntityManagerInterface $entityManager
    -persist(entity) prend une entité c'est une file d'attente pour creation ou update
    -remove(entity) il faut recuperer une entité pour la supprrimer
    -flush(entity) execution de la file d'attente remove ou persist
- Names ende by type like : TestFormType
- symfony console make:form

        {{ form_row(form.title ) }}
        {{ form_row(form.description) }}
        {{ form_row(form.pegi) }}
        {{ form_end(form) }}
        <button type="submit" class="btn btn-primary">Submit</button>
        {{ form_end(form) }}

- config/packages/twig.yaml
- form_themes: [ 'bootstrap_5_layout.html.twig' ]

- la validation se fait avec la class request de htttpFondation