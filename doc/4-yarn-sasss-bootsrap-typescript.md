npm install --global yarn

//encore est le web pack pour symfony

https://symfony.com/doc/current/frontend/encore/installation.html

composer require symfony/webpack-encore-bundle
yarn    //signifie : yarn install

on supprime tout ce qu'il y' a dans asset
creer à la racine le tsconfig.json qui doit contenir un objet vide {}
creer un dossier style dans assets, qui contient le fichier index.scss
creer un dossier script dans assets fichier index.ts  console.log('Hello World');

ajouter dans le webpack.config
delete
    .enableStimulusBridge('./assets/controllers.json')
decommenter
    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    .enableTypeScriptLoader()

plus besoin de toucher au fichier webpack.config

dans template/base.html.twig
encore_entry_link_tags  changer app en style
encore_entry_script_tags  changer en script

yarn watch 
erreur , recupérer la ligne de commande
yarn add sass-loader@^13.0.0 sass --dev

yarn watch 
erreur , recupérer la ligne de commande
yarn add typescript ts-loader@^9.0.0 --dev

tout doit etre bon
tout est compiler dans le public

// bootstrap ---------------
yarn add bootstrap
verifier dans nodemodules

dans le index.scss
@import "~bootstrap"; le tilde indique qu'il faut chercher dans le nodemodules

dans le index.ts
import"bootstrap";

importer les dépendances
yarn add @popperjs/core


// attention a bien dissocier les terminaux serveurs et commandes

deplantage aurelien pour sass
yarn add node-sass sass-loader --dev // une foispar projet
symfony run yarn dev



// lien dans le html
src="{{ asset('build/img/icons/newsletter.png') }}"

// et copie dans le webpack.config.js
.copyFiles([
        {from: './assets/img', to: 'img/[path][name].[ext]'},
        {from: './assets/script', to: 'script/[path][name].[ext]'},
    ])
.addEntry('script', './assets/script/index.js')
.addStyleEntry('style', './assets/style/index.scss')

// ensuite parametrer les liens pour le script et le style dans base.html.twig (dans les templates) 
{% block stylesheets %}
            {{ encore_entry_link_tags('style') }}
        {% endblock %}

        {% block javascripts %}
            {{ encore_entry_script_tags('script') }}
        {% endblock %}

yarn add file-loader@^6.0.0 --dev